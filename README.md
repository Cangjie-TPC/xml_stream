<div align="center">
<h1>xmlstream</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.4-brightgreen" style="display: inline-block;" />
<!-- <img alt="" src="https://img.shields.io/badge/cjcov-50%25-red" style="display: inline-block;" /> -->
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

提供 XML 操作相关的 `StAX` 风格接口，符合 [XML 1.0 规范](https://www.w3.org/TR/REC-xml/)，支持命名空间。

### 源码目录

```shell
.
├── README.md             #整体介绍
├── cjpm.toml             #CJPM 构建脚本
├── doc                   #文档目录，包括设计文档，API接口文档等
│   ├── design.md         #设计文档
│   └── feature_api.md    #特性接口文档
├── src                   #源码目录
│   └── xml_event.cj      #XmlEvent 枚举定义、辅助类定义
│   └── xml_reader.cj     #XML 反序列化器实现
│   └── xml_writer.cj     #XML 序列化器实现
│   └── xml_token.cj      #XML 词法解析相关内部工具类
│   └── xml_utils.cj      #XML 处理相关内部工具类

```

### 接口说明

主要类和函数接口说明，详见 [API](./doc/feature_api.md)。

## 使用说明
### 编译构建

描述具体的编译过程：

```shell
cjpm build
```

### 功能示例

序列化与反序列化示例，详见 [Design](./doc/design.md#展示示例)。

## 约束与限制
描述环境限制，版本限制，依赖版本等

## 开源协议

本项目基于 [Apache-2.0 License](./LICENSE) , 请自由的享受和参与开源。

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。