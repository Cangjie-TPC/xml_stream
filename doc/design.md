# xmlstream 库设计介绍

## 描述
`XML` 一般用于在应用程序中传输或存储类实例对象或数据库表记录。本库提供 `StAX` 风格的 XML 序列化、反序列化功能。支持用于 `.xml`、`.xsd` 文档的读写，不支持用于 `.dtd` 文档的读写。支持内部实体定义，不支持外部实体定义等其他文件类型定义定义。本库支持文档格式校验 `Well-Formed`，不支持文档有效性校验 `Valid`。支持 `XML` 命名空间。

## 展示示例

反序列化示例：

```cangjie
from std import io.ByteArrayStream

let xml = """
<?xml version="1.0" encoding="UTF-8"?>
<root>
    <item1>value1</item1>
    <item2>value2</item2>
</root>"""

main() {
    // 获取输入流
    let inputStream = ByteArrayStream()
    inputStream.write(xml.toArray())
    // 构造 Xml 反序列化器实例
    let reader = XmlReader(inputStream)
    // 遍历 xml 元素
    while (let Some(event) <- reader.peek()) {
        match (event) {
            // 读取元素开始标签中的元素名称
            case ElementStart => println("Element Start: ${reader.readElementStart().name}")
            // 读取文本内容
            case Text => println("Text: ${reader.readText()}")
            // 读取文档声明
            case Decl => reader.readDecl()
            // 读取元素结束标签中的元素名称
            case ElementEnd => println("Element End: ${reader.readElementEnd().name}")
            // 其他事件处理
            case _ => ()
        }
    }

    0
}
```

输出:

```text
Element Start: root
Element Start: item1
Text: value1
Element End: item1
Element Start: item2
Text: value2
Element End: item2
Element End: root
```

序列化示例：

```cangjie
from std import io.ByteArrayStream

main () {
    // 构造输出流
    let outputStream = ByteArrayStream()
    // 构造 Xml 序列化器实例
    let writer = XmlWriter(outputStream)
    // 设置元素缩进
    writer.elementIndent = "    "
    // 写文档声明
    writer.writeDecl()
    // 写根元素开始标签
    writer.writeElementStart("root")
    // 写第一个子元素开始标签
    writer.writeElementStart("item1")
    // 写第一个子元素的文本内容
    writer.writeText("value1")
    // 第一个子元素结束
    writer.writeElementEnd()
    // 写第二个子元素开始标签
    writer.writeElementStart("item2")
    // 写第二个子元素的文本内容
    writer.writeText("value2")
    // 第二个子元素结束
    writer.writeElementEnd()
    // 根元素结束
    writer.writeElementEnd()

    // 刷出缓冲区
    writer.flush()
    // 打印序列化结果
    println("${String.fromUtf8(outputStream.bytes())}")

    0
}
```

输出:

```text
<?xml version="1.0" encoding="UTF-8"?>
<root>
    <item1>value1</item1>
    <item2>value2</item2>
</root>
```