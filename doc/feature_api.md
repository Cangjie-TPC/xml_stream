# 枚举

## enum XmlEvent

```cangjie
public enum XmlEvent <: Equatable<XmlEvent> & Hashable & ToString {
    | Decl
    | Comment
    | ProcInst
    | DTD
    | ElementStart
    | ElementEnd
    | Attribute
    | Namespace
    | Text
    | CData
    | EntityRef
}
```

功能：反序列化时读到的 `XML` 事件枚举。

### Decl

```cangjie
Decl
```

功能：文档声明。形如: `<?xml version="1.0" encoding="UTF-8"?>`。

### Comment

```cangjie
Comment
```

功能：注释。形如: `<!--这是注释-->`。

### ProcInst

```cangjie
Comment
```

功能：处理指令。形如: `<?target instructions ?>`。

### DTD

```cangjie
Comment
```

功能：文件类型声明。形如: `<!DOCTYPE root [ <!ENTITY name value> ]>`。

> **说明：**
>
> 本库支持内部实体声明的自动解析，支持其他声明的自定义解析。

### ElementStart

```cangjie
ElementStart
```

功能：元素开始标签。形如: `<root>`。

### Attribute

```cangjie
Attribute
```

功能：元素属性。形如: `<root name="value">`。

> **说明：**
>
> 属性存在于元素开始签之内，属性值可以使用 `'` 或 `"` 包裹。

### Namespace

```cangjie
Namespace
```

功能：命名空间声明。形如: `<root xmlns="URI" xmlns:prefix="URI2">`。

> **说明：**
>
> 命名空间声明是一种特殊的属性，用于在元素及其子元素的内容中限定元素或属性的名称的命名空间。
> 命名空间主要用于文档合法性(`Valid`) 校验，对于相同名称的元素，校验时可以基于不同的命名空间来区别。

> **注意：**
>
> 本库不会在解析时进行合法性校验。
> 文档合法性校验一般需要通过解析 `.dtd` 或 `.xsd` 文件以获取文档类型定义，根据文档类型定义来对解析到的 `XML` 元素进行合法性校验。

### ElementEnd

```cangjie
ElementEnd
```

功能：元素结束标签。形如: `</root>` 或 `/>`。

> **说明：**
>
> 对于*空元素标签*，`/>` 将被解析为*元素结束标签*事件。即*空元素标签* `<root/>` 将被解析为*元素开始标签事件*和*元素结束标签事件*两个事件。

### Text

```cangjie
Text
```

功能：元素内容文本。元素内容中不被识别为*标签*的部分，将被解析为此事件。

### CData

```cangjie
CData
```

功能：不解析的字符数据。形如：`<![CDATA[...]]>`。

### EntityRef

```cangjie
EntityRef
```

功能：实体引用。形如: `&name;`。

# 接口

## interface XmlDeserializable\<T\>

```cangjie
public interface XmlDeserializable<T> {
    static func fromXml(r: XmlReader): T
}
```

功能：提供实现 `XML` 反序列化功能的接口。

### static func fromXml(XmlReader)

```cangjie
static func fromXml(r: XmlReader): T
```

功能：反序列化出一个类对象实例。

参数：

- r: [XmlReader](#class-xmlreader) - `XML` 反序列化器，提供反序列化 `XML` 对象的方法。

返回值：

- T - 反序列化出来的对象实例。

## interface XmlSerializable

```cangjie
public interface XmlSerializable {
    func toXml(w: XmlWriter): Unit
}
```

功能：提供实现 `XML` 序列化功能的接口。

### func toXml(XmlWriter)

```cangjie
func toXml(w: XmlWriter): Unit
```

功能：将对象实例进行序列化。

参数:

- w: [XmlWriter](#class-xmlwriter) - `XML` 序列化器，提供序列化 `XML` 对象的方法。

# 类

## class XmlReader

### init(InputStream)

```cangjie
public init(input: InputStream)
```

功能：构造器。

### func readObject\<T\>()

```cangjie
public func readObject<T>(): T where T <: XmlDeserializable<T> {
```

功能：反序列化一个 XML 对象实例出来。

返回值：对象实例。

### func peek()

```cangjie
public func peek(): ?XmlEvent {
```

功能：读取一个 XML 事件。

返回值：[XmlEvent](#enum-xmlevent) 实例。

当读完根元素的结束标签后，此方法此后将返回 None。

异常：

- XmlException - XML 文档不符合规范。

### func skip()

```cangjie
public func skip(): Unit {
```

功能：跳过一个 XML 事件。

异常：

- XmlException - XML 文档不符合规范。

### func readDecl()

```cangjie
public func readDecl(): (String, ?String, ?Bool) {
```

功能：读文件声明。

返回值：(version, encoding, standalone) 三元组。

调用此方法的前置条件: [peek](#func-peek) 返回值为：[Decl](#decl)。

异常：

- XmlException - XML 文档不符合规范。
- XmlStatusException - 当前事件不支持此方法调用。

### func readElementStart()

```cangjie
public func readElementStart(): QName
```

功能：读元素开始标签。

返回值：[QName](#class-qname) 完全限定名称。**暂不支持命名空间解析。**

调用此方法的前置条件: [peek](#func-peek) 返回值为：[ElementStart](#elementstart)。

异常：

- XmlException - XML 文档不符合规范。
- XmlStatusException - 当前事件不支持此方法调用。

### func readElementStart(String,?String)

```cangjie
public func readElementStart(name: String, namespace!: ?String=None): Unit
```

功能：读元素开始标签并校验名称。

参数：

- name: String - 元素名称。
- namespace: ?String - 接口预留参数。

调用此方法的前置条件: [peek](#func-peek) 返回值为：[ElementStart](#elementstart)。

异常：

- XmlException - XML 文档不符合规范，或名称不匹配。
- XmlStatusException - 当前事件不支持此方法调用。

### func readElementText(String,?String)

```cangjie
public func readElementText(name: String, namespace!: ?String=None): String {
```

功能：读元素开始标签以及文本内容并校验名称。

参数：

- name: String - 元素名称。
- namespace: ?String - 接口预留参数。

返回值：元素的文本内容。

调用此方法的前置条件: [peek](#func-peek) 返回值为：[ElementStart](#elementstart)。

异常：

- XmlException - XML 文档不符合规范，或名称不匹配。
- XmlStatusException - 当前事件不支持此方法调用，或元素内容不是纯文本类元素。

### func readElementEnd()

```cangjie
public func readElementEnd(): QName {
```

功能：读元素结束标签。

返回值：[QName](#class-qname) 完全限定名称。**暂不支持命名空间解析。**

调用此方法的前置条件: [peek](#func-peek) 返回值为：[ElementEnd](#elementend)。

异常：

- XmlException - XML 文档不符合规范。
- XmlStatusException - 当前事件不支持此方法调用。

### func readElementEnd(String,?String)

```cangjie
public func readElementEnd(name: String, namespace!: ?String=None): Unit {
```

功能：读元素结束标签并校验元素名称。

参数：

- name: String - 元素名称。
- namespace: ?String - 接口预留参数。

调用此方法的前置条件: [peek](#func-peek) 返回值为：[ElementEnd](#elementend)。

异常：

- XmlException - XML 文档不符合规范，或元素名称不匹配。
- XmlStatusException - 当前事件不支持此方法调用。

### func readText()

```cangjie
public func readText(): String {
```

功能：读文本内容。

返回值：文本内容。

调用此方法的前置条件: [peek](#func-peek) 返回值为：[ElementEnd](#elementend) | [Text](#text) | [CData](#cdata) | [EntityRef](#entityref)。

异常：

- XmlException - XML 文档不符合规范。
- XmlStatusException - 当前事件不支持此方法调用。

### func readCData()

```cangjie
public func readCData(): String {
```

功能：读 CDATA 文本内容。

返回值：文本内容。

调用此方法的前置条件: [peek](#func-peek) 返回值为：[CData](#cdata)

异常：

- XmlException - XML 文档不符合规范。
- XmlStatusException - 当前事件不支持此方法调用。

## class XmlWriter

### init(OutputStream)

```cangjie
public XmlWriter(private let output: OutputStream)
```

功能：构造器。

参数：

- output: OutputStream - 输出流。

### mut prop elementIndent

```cangjie
public mut prop elementIndent: String
```

功能：元素缩进字符。必须由 '\t' 或者 ' ' 组成。设置为空字符串时，表示不进行缩进。

异常：

- IllegalArgumentException - 当设置的值包含 `\t` 或 ` ` 以外的值时，抛出此异常。

### func writeObject\<T\>(T)

```cangjie
public func writeObject<T>(v: T): XmlWriter where T <: XmlSerializable {
```

功能：将 XML 对象序列化。

参数：

- v: T - 支持序列化为 XML 格式的对象实例。

返回值：[XmlWriter](#class-xmlwriter) 对象自身，以供链式调用。

### func flush()

```cangjie
public func flush(): Unit
```

功能：将序列化结果刷出缓冲区。

### func writeDecl(String,String,?Bool)

```cangjie
public func writeDecl(version!: String="1.0", encoding!: String="UTF-8", standalone!: ?Bool=None): XmlWriter {
```

功能：写文件声明。

参数：

- version: String - XML 版本，应为 "1.0"。
- encoding: String - 文件编码，当前仅支持 "UTF-8"，其他编码格式的行为未定义。
- standalone: ?Bool - 文档是否独立标识。

返回值：[XmlWriter](#class-xmlwriter) 对象自身，以供链式调用。

异常：

- [XmlStatusException](#class-xmlstatusexception) - 当在不是文件开始处调用此方法时，抛出此异常。

### func writeElementStart(String,?String)

```cangjie
public func writeElementStart(name: String, prefix!: ?String=None): XmlWriter {
```

功能：写元素开始标签。

参数：

- name: String - 元素名称。
- prefix: ?String - 接口预留参数。

返回值：[XmlWriter](#class-xmlwriter) 对象自身，以供链式调用。

### func writeElementEnd(Bool)

```cangjie
public func writeElementEnd(emptyElement!: Bool=false): XmlWriter {
```

功能：写元素结束标签。结束标签中的名称将自动填充为最近的开始标签中的名称。

参数：

- emptyElement: Bool - 是否为空元素标签。

返回值：[XmlWriter](#class-xmlwriter) 对象自身，以供链式调用。

> **说明：**
>
> 若从元素开始标签之后仅写了元素属性或者命名空间，则可使用 emptyElement 参数写空标签，否则此参数无效。

### func writeText(String)

```cangjie
public func writeText(text: String): XmlWriter {
```

功能：写文本内容。

参数：

- text: String - 文本内容值。

返回值：[XmlWriter](#class-xmlwriter) 对象自身，以供链式调用。

### func writeCData(String)

```cangjie
public func writeCData(text: String): XmlWriter {
```

功能：写 CDATA 标签。

参数：

- text: String - CDATA 中的文本内容。

返回值：[XmlWriter](#class-xmlwriter) 对象自身，以供链式调用。

## class QName

```cangjie
public class QName
```

功能：完全限定名称。包含前缀、命名空间、名称。

### init(String, ?String, ?String)

```cangjie
public QName(private let _name: String, private let _prefix: ?String, private let _namespace: ?String) {}
```

功能：构造器。

参数：

- _name: String - 名称。
- _prefix: String - 命名空间前缀。
- _namespace: String - 命名空间值，一般是 URI 形式的值。

### prop name

```cangjie
public prop name: String
```

功能：获取名称值。

### prop prefix

```cangjie
public prop prefix: ?String
```

功能：获取命名空间前缀。

### prop namespace

```cangjie
public prop namespace: ?String
```

功能：获取命名空间值。

### 

# 异常

## class XmlException

```cangjie
open public class XmlException <: Exception
```

功能：此类用于表示 `XML` 相关异常。 

### init()

```cangjie
public init()
```

功能：创建一个 `XmlException` 异常实例。

### init(String)

```cangjie
public init(msg: String)
```

功能：创建一个 `XmlException` 异常实例，并指定异常信息。

参数：

- msg: String - 异常信息。

## class XmlStatusException

```cangjie
public class XmlStatusException <: XmlException
```

功能：此类为 `XML` 状态异常。

当在不是文件开始的位置写文件声明，或在读到注释事件时调用读取元素的方法等非法情况时，抛出此异常。

### init()

```cangjie
public init()
```

功能：创建一个 `XmlStatusException` 异常实例。

### init(String)

```cangjie
public init(msg: String)
```

功能：创建一个 `XmlStatusException` 异常实例，并指定异常信息。

参数:

- msg: String - 异常信息。