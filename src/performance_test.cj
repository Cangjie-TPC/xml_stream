package xmlstream

import std.unittest.testmacro.{Test, Bench}

@Test
class PerfTest {

    static private let xml = """
<?xml version="1.0" encoding="utf-8"?>
<service>
    <syshead>
        <service_code>302000006</service_code>
        <service_scn>03</service_scn>
        <file_flag>0</file_flag>
        <file_path><![CDATA[http://example.org&arg1=v1]]></file_path>
        <auth_flag/>
    </syshead>
    <apphead>
        <pg_up_or_pg_dn></pg_up_or_pg_dn>
        <page_total_num></page_total_num>
    </apphead>
    <body>
        <whole_part_seq_no>20240328193211204871</whole_part_seq_no>
        <cur_page_no>001</cur_page_no>
        <cur_page_name>初始页</cur_page_name>
    </body>
</service>"""

    private let input = ResetableInputStream(xml.toArray())

    @Bench
    func readerTest(): Unit {
        input.reset()
        let reader = XmlReader(input)

        while (let Some(event) <- reader.peek()) {
            reader.skip()
        }
    }
}

class ResetableInputStream <: InputStream {
    let data: Array<Byte>
    var idx = 0

    public init(data: Array<Byte>) {
        this.data = data.clone()
    }

    public func read(buffer: Array<Byte>): Int64 {
        let size = data.size - idx
        if (size <= 0) {
            return -1
        }

        let copyLen = if (buffer.size > size) { size } else { buffer.size }
        data.copyTo(buffer, idx, 0, copyLen)
        idx += copyLen
        return copyLen
    }

    public func reset(): Unit {
        idx = 0
    }
}